

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import java.util.ArrayList;
import java.util.List;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;
import java.net.URL;
import java.util.Enumeration;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class TrafficIncidentReportServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<String> history;
	
	public TrafficIncidentReportServlet() {
		history = new ArrayList<>();
	}

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) 
                    throws ServletException, IOException
    {
		Enumeration<String> params = request.getParameterNames();
		Cookie cookies [] = request.getCookies();
		String session = null;
		String publishedDate = null;
		String issueReported = null;
		if(params.hasMoreElements()) {
			String param1 = params.nextElement();
			if(!param1.equals("session") && params.hasMoreElements()) {
				response.getWriter().println("Error. Pass only one parameter");
				return;
			}
			else if(param1.equals("session")) {
				session = request.getParameter("session");
			}
			else if(param1.equals("published_date")) {
				publishedDate = request.getParameter("published_date");
			}
			else if(param1.equals("issue_reported")) {
				issueReported = request.getParameter("issue_reported");
			}
		}
	
		if(session != null) {	
			if(session.toLowerCase().equals("start")) {	
				Cookie cookie = new Cookie("inSession", "true");
				cookie.setDomain("localhost");
                cookie.setPath("/assignment1" + request.getServletPath());
                cookie.setMaxAge(-1);
				response.addCookie(cookie);
				history = new ArrayList<>();
				printHistory(request, response);
			}
			else if(session.toLowerCase().equals("end")) {	
				for (int i = 0; i < cookies.length; ++i) {
					Cookie cookie = cookies[i];
					cookie.setValue("");
					cookie.setMaxAge(0);
					cookie.setDomain("localhost");
					cookie.setPath("/assignment1" + request.getServletPath());
					response.addCookie(cookie);
				}
				printHistory(request, response);
				history = new ArrayList<>();
			}
			else {
				response.getWriter().println("Incorrect value specified");
				return;
			}
		}
		else {
			//If there is a cookie, we are in a session. Print history. Otherwise just print url and don't add it to history
			if(cookies != null)
				printHistory(request, response);
			else {
				response.getWriter().println("History\n");
				response.getWriter().println(request.getRequestURL() + "?" + request.getQueryString());
			}
		}
			
		printData(response, publishedDate, issueReported);
		

    }
	
	private void printHistory(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		history.add(request.getRequestURL() + "?" + request.getQueryString());
		response.getWriter().println("History");
		for(int i = 0; i < history.size(); ++i)
			response.getWriter().println(history.get(i));
	}
	
	private void printData(HttpServletResponse response, String publishedDate, String issueReported)  throws ServletException, IOException{
		response.getWriter().println("\nData");
		if(issueReported != null) {
			int parseCount = parsing("issue_reported", issueReported);
			response.getWriter().println("Number of issues of type " + issueReported + ":" + parseCount);
		}
		else if(publishedDate != null) {
			int parseCount = parsing("published_date", publishedDate);
			response.getWriter().println("Number of issues on " + publishedDate + ":" + parseCount);
		}
	}
	
	private int parsing(String key, String value) {
		int count = 0;
		
		DocumentBuilderFactory builderFactory =
		        DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		try {
		    builder = builderFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
		    e.printStackTrace();  
		}
		
		Document document = null;
		try {
			document = builder.parse(new URL("http://www.cs.utexas.edu/~devdatta/traffic_incident_data.xml").openStream());
		} catch (SAXException e) {
		    e.printStackTrace();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
		Element rootElement = document.getDocumentElement();
		Queue<Element> q = new LinkedList<Element>();
		q.add(rootElement);
		
		while(!q.isEmpty()) {
			Element e = (Element) q.remove();			
			if (e.getNodeName().equals("ds:" + key)) {
		    	String nodeValue = e.getTextContent().toLowerCase();
		    	if(key.equals("published_date") && nodeValue.contains(value)) {
		    		++count;
		    	}
		    	else if(key.equals("issue_reported") && nodeValue.equals(value)) {
		    		++count;
		    	}
		    }
			NodeList nodes = e.getChildNodes();
			for(int i=0; i<nodes.getLength(); i++) {
				  Node node = nodes.item(i);
				  if(node instanceof Element) {
					  q.add((Element) node);
				    }
				  }
			}
		
		return count;
	}

}
